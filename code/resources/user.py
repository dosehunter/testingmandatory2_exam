import argparse

from flask_restful import Resource, reqparse
from models.user import UserModel


class UserRegister(Resource):
    MIN_LEN = 6
    MAX_LEN = 80

    #def range_limit_str(self, s):
    #    if len(s) < self.MIN_LEN or len(s) > self.MAX_LEN:
    #        print("ERROR")
    #        return None
    #        #raise argparse.ArgumentTypeError("Argument must be < " + str(80) + "and > " + str(6))
    #    return s

    parser = reqparse.RequestParser()
    parser.add_argument('username',
                        type=str,
                        required=True,
                        help="This field cannot be blank."
                        )
    parser.add_argument('password',
                        type=str,
                        required=True,
                        help="This field cannot be blank."
                        )
    parser.add_argument('email',
                        type=str,
                        required=True,
                        help="This field cannot be blank."
                        )

    def check_pw_len(self, data):
        if len(data['password']) < self.MIN_LEN:
            return "short"

        if len(data['password']) > self.MAX_LEN:
            return "long"

    def check_len(self, data):
        if len(data['username']) < self.MIN_LEN:
            if self.check_pw_len(data) == "short":
                return {"message": "Username and password is too short (below 6 char)"}, 400
            return {"message": "Username is too short (below 6 char)"}, 400

        if len(data['username']) > self.MAX_LEN:
            if self.check_pw_len(data) == "long":
                return {"message": "Username and password is too long (exceed 80 char)"}, 400
            return {"message": "Username is too long (exceed 80 char)"}, 400

        if len(data['password']) < self.MIN_LEN:
            return {"message": "Password is too short (below 6 char)"}, 400

        if len(data['password']) > self.MAX_LEN:
            return {"message": "Password is too long (exceed 80 char)"}, 400

        if len(data['email']) < self.MIN_LEN:
            return {"message": "Email is too short (below 6 char)"}, 400

        if len(data['email']) > self.MAX_LEN:
            return {"message": "Email is too long (exceed 80 char)"}, 400

        return None

    def post(self):
        data = UserRegister.parser.parse_args()

        len_msg = self.check_len(data)

        if len_msg:
            return len_msg

        if UserModel.find_by_username(data['username']):
            return {"message": "A user with that username already exists"}, 400

        if UserModel.find_by_email(data['email']):
            return {"message": "A user with that email already exists"}, 400

        user = UserModel(data['username'], data['password'], data['email'])
        user.save_to_db()

        return {"message": "User created successfully."}, 201