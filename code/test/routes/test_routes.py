import pytest


class TestRoutes:

    @pytest.mark.parametrize("route, status_code, data, expect_error", [
        ("/",               200,  b"Testesting App",                    False),
        ("/photo/1",        200,  None,                                 False),
        ("/photo/2",        200,  None,                                 False),
        ("/test_page",      200,  b"THIS IS PURELY FOR TESTING ROUTES", False),
        # Below are invalid:
        ("/photo/0",        404,  None,                                 True),
        ("/photo/10",       404,  None,                                 True),
    ])
    def test_routes(self, test_client, init_database, route, status_code, data, expect_error):
        """
        Will test routes for application

        :param test_client: Flask app client for testing (Fixture)
        :param init_database: Setup database if not already done (Fixture)
        :param route: Url route (Homepage = /)
        :param status_code: HTTP status code
        :param data: Data which can be found on the page
        :param expect_error: Whether an error is expected or not
        """
        # Invalid routes should (currently) raise a typeerror as image is not found
        if expect_error:
            with pytest.raises(TypeError) as e_info:
                response = test_client.get(route)
        else:
            response = test_client.get(route)
            assert response.status_code == status_code
            if data:
                assert data in response.data
