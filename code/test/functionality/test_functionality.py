import pytest
from security import authenticate, identity


class TestUserFunctionality:
    @pytest.mark.parametrize("username, password, email, status_code, data", [
        ("martin",          "mpassw",       "ma@1.c",       201, b"User created successfully."),
        ("MARTIN",          "MPASSW",       "MA@2.C",       201, b"User created successfully."),
        ("MaRtI3",          "MpASsW",       "mA@3.C",       201, b"User created successfully."),
        # Testing boundary of 80 char:
        ("martin12"*10,     "mpassw",       "ma@4.c",       201, b"User created successfully."),
        ("marti5",          "mapasswo"*10,  "ma@5.c",       201, b"User created successfully."),
        ("martin32"*10,     "mapasswo"*10,  "ma@6.c",       201, b"User created successfully."),
        ("martin45"*10,     "mapasswo"*10,  "mart@5.c"*10,  201, b"User created successfully."),
        # Duplicate, same username as entry 1:
        ("martin",          "mpassw",       "mart@42.com",  400, b"A user with that username already exists"),
        # Duplicate, same email as entry 2:
        ("martin42",        "mpassw",       "MA@2.C",       400, b"A user with that email already exists"),
        # Testing boundaries + 80 char
        ("martin987"*9,     "mapassword1",  "Alice_mail_1@mail.com", 400, b"Username is too long (exceed 80 char)"),
        ("Alice1",          "toolongpw"*9,  "Alice_mail_2@mail.com", 400, b"Password is too long (exceed 80 char)"),
        ("martin987"*9,     "toolongpw"*9,  "Alice_mail_1@mail.com", 400, b"Username and password is too long (exceed 80 char)"),
        ("Alice2",          "AlicePw",      "alice@mai"*9,           400, b"Email is too long (exceed 80 char)"),
        # Testing boundaries below 6:
        ("Bob",               "goodPwd",   "bobs_good_1@mail.com", 400, b"Username is too short (below 6 char)"),
        ("Bobs_good_uname",   "bob12",     "bobs_good_2@mail.com", 400, b"Password is too short (below 6 char)"),
        ("Bob2",              "bob12",   "bobs_good_3@mail.com",   400, b"Username and password is too short (below 6 char)"),
        ("bobs_good_uname_3", "AlicePw", "bo@ma",                  400, b"Email is too short (below 6 char)"),
    ])
    def test_registration(self, test_client, init_database, username, password, email, status_code, data):
        """
        Test registering users

        The given username, password and email will be registered and inserted into the database.
        :param test_client: Flask app client for testing (Fixture)
        :param init_database: Setup database if not already done (Fixture)
        :param username: String, max 80 char
        :param password: String, max 80 char
        :param email: String, max 80 char
        :param status_code: Expected return status code
        :param data: Expected data in response
        """
        response = test_client.post('/register',
                                    data=dict(username=username,
                                              password=password,
                                              email=email),
                                    follow_redirects=True)
        assert response.status_code == status_code
        if data:
            assert data in response.data

    @pytest.mark.parametrize("username, password, status_code", [
        ("test_user_1", "test_password_1", 200),
        ("TEST_USER_2", "TEST_PASSWORD_2", 200),
        ("Test_User_3", "Test_Password_3", 200),
        ("test_user_1", "WRONGPASSWORD", 401),
        ("TEST_USER_2", "test_password_1", 401),
        ("test_user_", "WRONGPASSWORD", 401),
        ("test_user_1", "", 401),
        ("test_user_42", "", 401),
        ("test_user_42", "wrongpass"*9, 401),
    ])
    def test_login(self,test_client, init_database, username, password, status_code):
        """
        Test login with usernames and passwords

        :param test_client: Flask app client for testing (Fixture)
        :param init_database: Setup database if not already done (Fixture)
        :param username: String, username of the user
        :param password: String, password belonging to the user
        :param status_code: HTTP status code
        """
        response = test_client.post('/login',
                                    data=dict(uname=username,
                                              psw=password),
                                    follow_redirects=True)
        assert response.status_code == status_code
        #TODO assert b"Welcome" in response
        print(response.data)

    @pytest.mark.parametrize("username, password, email, uid, valid", [
        # Valid users
        ("test_user_1", "test_password_1", "test_mail_1@gmail.com", 1, True),
        ("TEST_USER_2", "TEST_PASSWORD_2", "TEST_MAIL_2@GMAIL.COM", 2, True),
        ("Test_User_3", "Test_Password_3", "Test_Mail_3@Gmail.Com", 3, True),
        # Invalid users
        ("Invalid_test_user", "NicePassword", "Test_Mail_3@Gmail.Com", 13, False),
        # Test matching username wrong password
        ("test_user_1", "Wrongpassword", "Test_Mail_3@Gmail.Com", 1, False),
        # Test case sensitivity:
        ("test_user_1", "Test_password_1", "Test_Mail_3@Gmail.Com", 1, False),
        # Test similar password
        ("test_user_1", "test_password_", "Test_Mail_3@Gmail.Com", 1, False),
    ])
    def test_authenticate(self, test_client, init_database, username, password, email, uid, valid):
        """
        Test authentication of users

        :param test_client: Flask app client for testing (Fixture)
        :param init_database: Setup database if not already done (Fixture)
        :param username: String, max 80 char
        :param password: String, max 80 char
        :param email: String, max 80 char
        :param uid: id of returned user
        :param valid: whether testing a valid user or not
        """
        test_user = authenticate(username, password)
        if valid:
            assert test_user.id == uid
            assert test_user.username == username
            assert test_user.password == password
            assert test_user.email == email
        else:
            assert test_user is None

        print(test_user)  # If anything goes wrong

    @pytest.mark.parametrize("payload, username, password, email, valid", [
        # Valid payloads using mocked payloads:
        ({'identity': 1}, "test_user_1", "test_password_1", "test_mail_1@gmail.com", True),
        ({'identity': 2}, "TEST_USER_2", "TEST_PASSWORD_2", "TEST_MAIL_2@GMAIL.COM", True),
        ({'identity': 3}, "Test_User_3", "Test_Password_3", "Test_Mail_3@Gmail.Com", True),
        # Invalid payloads, invalid ids above 0 should be very high/or db should reset before this
        ({'identity': 1050}, "test_user_1", "test_password_1", "test_mail_1@gmail.com", False),
        ({'identity': 20050}, "test_user_1", "test_password_1", "test_mail_1@gmail.com", False),
        ({'identity': 0}, "test_user_1", "test_password_1", "test_mail_1@gmail.com", False),
        ({'identity': -1}, "test_user_1", "test_password_1", "test_mail_1@gmail.com", False),
    ])
    def test_identity(self, test_client, init_database, payload, username, password, email, valid):
        """

        :param test_client: Flask app client for testing (Fixture)
        :param init_database: Setup database if not already done (Fixture)
        :param payload: Dictionary with mocked data
        :param username: String, max 80 char
        :param password: String, max 80 char
        :param email: String, max 80 char
        :param valid: Boolean, whether identity is valid or not
        """
        test_user = identity(payload)
        if valid:
            assert test_user.username == username
            assert test_user.password == password
            assert test_user.email == email
        else:
            assert test_user is None

        print(test_user)  # If anything goes wrong

class TestPhotoFunctionality:
    @pytest.mark.parametrize("photo_name, data, visible, user_id", [
        ("Funny_Robot", open("code/test/abs_robot.jpg", 'rb'), 1, 1),
        ("Funny_cat", open("code/test/Cat.jpg", 'rb'), 0, 3),
    ])
    def test_invalid_photo_upload(self, test_client, init_database, photo_name, data, visible, user_id):
        """
        Will raise an exception as we are not logged in and have no security token.

        :param test_client: Flask app client for testing (Fixture)
        :param init_database: Setup database if not already done (Fixture)
        :param photo_name: String, name of photo
        :param data: Binary data of image
        :param visible: Int, whether visible (1) or not (0)
        :param user_id: Int, user trying to upload
        """
        with pytest.raises(AttributeError):
            response = test_client.post('/photo',
                                        data=dict(photo_name=photo_name,
                                                  data=data),
                                        follow_redirects=True)
