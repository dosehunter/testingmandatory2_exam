import pytest
from models.user import UserModel
from models.photo import PhotoModel
import time

class TestUserModel:

    @pytest.mark.parametrize("username, password, email", [
        ("test_username","test_password","test_mail@gmail.com"),
        ("8279301238","123879014","1231412@gmail.com"),
        ("test_usernametest_usernametest_usernametest_usernametest_usernametest_username12","test_password","test_mail@gmail.com"),
        ("test_username","test_password","test_mail@gmail.com"),
    ])
    def test_user_model(self, username, password, email):
        """
        Test of UserModel

        :param username: String, username to test
        :param password: String, password to test
        :param email: String, email to test
        """
        new_user = UserModel(username, password, email)
        assert new_user.username == username
        assert new_user.password == password
        assert new_user.email == email

    @pytest.mark.parametrize("user_id, valid", [
        (1, True),
        (2, True),
        (3, True),
        (101, False),
        (-1, False),
    ])
    def test_find_user_by_id(self, test_client, init_database, get_users, user_id, valid):
        """
        Testing find photo function

        Photos are added via init_database
        :param test_client: Flask app client for testing (Fixture)
        :param init_database: Setup database if not already done (Fixture)
        :param get_users: List of dicts (Fixture)
        :param user_id Int, id of user
        :param valid Whether id should exist
        """
        mock_user = UserModel("FAKE_USER", "FAKE_PASSWORD", "FAKE@EMAIL")
        test_user = mock_user.find_by_id(user_id)
        if valid:
            real_user = next(item for item in get_users if item["id"] == user_id)

            assert test_user.id == user_id
            assert test_user.username == real_user['username']
            assert test_user.password == real_user['password']
            assert test_user.email == real_user['email']
        else:
            assert test_user is None

    @pytest.mark.parametrize("username, valid", [
        ("test_user_1", True),
        ("TEST_USER_2", True),
        ("Test_User_3", True),
        ("test_user_", False),
        ("test_user_11", False),
    ])
    def test_find_user_by_username(self, test_client, init_database, get_users, username, valid):
        """
        Testing find photo function

        Photos are added via init_database
        :param test_client: Flask app client for testing (Fixture)
        :param init_database: Setup database if not already done (Fixture)
        :param get_users: List of dicts (Fixture)
        :param username String, username of user
        :param valid Whether id should exist
        """
        mock_user = UserModel("FAKE_USER", "FAKE_PASSWORD", "FAKE@EMAIL")
        test_user = mock_user.find_by_username(username)
        if valid:
            real_user = next(item for item in get_users if item["username"] == username)

            assert test_user.username == username
            assert test_user.id == real_user['id']
            assert test_user.password == real_user['password']
            assert test_user.email == real_user['email']
        else:
            assert test_user is None

    @pytest.mark.parametrize("email, valid", [
        ("test_mail_1@gmail.com", True),
        ("TEST_MAIL_2@GMAIL.COM", True),
        ("Test_Mail_3@Gmail.Com", True),
        ("test_mail_1@gmail.co", False),
        ("test_mail_1@gmail.comm", False),
    ])
    def test_find_user_by_email(self, test_client, init_database, get_users, email, valid):
        """
        Testing find photo function

        Photos are added via init_database
        :param test_client: Flask app client for testing (Fixture)
        :param init_database: Setup database if not already done (Fixture)
        :param get_users: List of dicts (Fixture)
        :param email String, email of user
        :param valid Whether id should exist
        """
        mock_user = UserModel("FAKE_USER", "FAKE_PASSWORD", "FAKE@EMAIL")
        test_user = mock_user.find_by_email(email)
        if valid:
            real_user = next(item for item in get_users if item["email"] == email)

            assert test_user.email == email
            assert test_user.id == real_user['id']
            assert test_user.password == real_user['password']
            assert test_user.username == real_user['username']
        else:
            assert test_user is None

    @pytest.mark.parametrize("username, password, email", [
        ("created_test_user_1", "good_password", "created_user@mail.com"),
        ("created_test_user_2", "Good", "created_user_2@mail.com"),
        ("crU", "good_password_3", "created_user_3@mail.com"),
    ])
    def test_save_to_db(self, test_client, init_database, username, password, email):
        test_user = UserModel(username, password, email)
        test_user.save_to_db()
        saved_user = test_user.find_by_username(username)

        assert saved_user.username == username
        assert saved_user.password == password
        assert saved_user.email == email
        assert saved_user.id is not 0
        assert saved_user.id > 0

class TestPhotoModel:

    @pytest.mark.parametrize("photo_name, data, visible, user_id", [
        ("Robot",       "DATA_PLACEHOLDER_1",                   1,  1),
        ("Cat",         "DATA_PLACEHOLDER_2",                   0,  2),
        ("Robot_cat",   "DATA_PLACEHOLDER_3",                   1,  3),
        ("Robot",       "DATA_PLACEHOLDER_4",                   0,  2),
        ("Robot_img",   open("code/test/Cat.jpg",'rb'),         0,  2),
        ("cat_img",     open("code/test/abs_robot.jpg", 'rb'),  1,  2),
    ])
    def test_photo_model(self, photo_name, data, visible, user_id):
        """
        Test of PhotoModel

        :param photo_name: String, photo name
        :param data: String/Binary of image
        :param visible: Whether visible (1) or not (0)
        :param user_id: Id of user uploading
        """
        new_photo = PhotoModel(photo_name, data, visible, user_id)
        assert new_photo.photo_name == photo_name
        assert new_photo.data == data
        assert new_photo.visible == visible
        assert new_photo.user_id == user_id

    @pytest.mark.parametrize("photo_id, valid", [
        (1,     True),
        (2,     True),
        (3,     True),
        (101,   False),
        (-1,   False),
    ])
    def test_find_photo_by_id(self, test_client, init_database, photo_id, get_photos, valid):
        """
        Testing find photo function

        Photos are added via init_database
        :param test_client: Flask app client for testing (Fixture)
        :param init_database: Setup database if not already done (Fixture)
        :param photo_id Int, id of photo
        :param get_photos: List of dicts (Fixture)
        :param valid Whether id should exist
        """
        mock_photo = PhotoModel("TEST_PHOTO", "FAKE_DATA", 1, 1)
        test_photo = mock_photo.find_by_id(photo_id)
        if valid:
            real_photo = next(item for item in get_photos if item["id"] == photo_id)

            assert test_photo.id == photo_id
            assert test_photo.user_id == real_photo['uid']
            assert test_photo.photo_name == real_photo['photo_name']
            assert test_photo.visible == real_photo['visible']
        else:
            assert test_photo is None

    @pytest.mark.parametrize("photo_name, valid", [
        ('test_photo_1', True),
        ('test_photo_2', True),
        ('test_photo_3', True),
        ('test_photo', False),
        ('No_PHOTO', False)
    ])
    def test_find_photo_by_name(self, test_client, init_database, photo_name, get_photos, valid):
        """
        Testing find photo function

        Photos are added via init_database
        :param test_client: Flask app client for testing (Fixture)
        :param init_database: Setup database if not already done (Fixture)
        :param photo_name String, name of photo
        :param get_photos: List of dicts (Fixture)
        """
        mock_photo = PhotoModel("TEST_PHOTO", "FAKE_DATA", 1, 1)
        test_photo = mock_photo.find_by_name(photo_name)
        if valid:
            real_photo = next(item for item in get_photos if item["photo_name"] == photo_name)

            assert test_photo.photo_name == photo_name
            assert test_photo.user_id == real_photo['uid']
            assert test_photo.id == real_photo['id']
            assert test_photo.visible == real_photo['visible']
        else:
            assert test_photo is None

    @pytest.mark.parametrize("uid, amount_of_photos", [
        (1, 1),
        (2, 1),
        (3, 2),
        (-1, 0),
        (101, 0),
    ])
    def test_find_all_by_user(self, test_client, init_database, get_photos, uid, amount_of_photos):
        """
        Testing if the model finds all photos a user have uploaded

        :param test_client: Flask app client for testing (Fixture)
        :param init_database: Setup database if not already done (Fixture)
        :param get_photos: List of dicts (Fixture)
        :param uid: Int, id of user who uploaded photo
        :param amount_of_photos: Expected amount of owned photos
        """
        mock_photo = PhotoModel("TEST_PHOTO", "FAKE_DATA", 1, 1)
        test_photos = mock_photo.find_all_by_user_id(uid)

        assert len(test_photos) == amount_of_photos

        for pho in test_photos:
            real_photo = next(item for item in get_photos if item["uid"] == uid)
            assert pho.user_id == real_photo['uid']

    @pytest.mark.parametrize("photo_name, data, visible, uid", [
        ("Cool cat",    b"FAKE PHOTO DATA",  1, 1),
        ("CC",          b"FAKE PHOTO DATA",  1, 2),
        ("Nice robot",  b"FAKE PHOTO DATA",  0, 3),
    ])
    def test_save_to_db(self, test_client, init_database, photo_name, data, visible, uid):
        """
        Testing the save function of the photo model, no restrictions here

        :param test_client: Flask app client for testing (Fixture)
        :param init_database: Setup database if not already done (Fixture)
        :param photo_name: Name of the photo be be saved
        :param data: String/Binary of image
        :param visible: Whether visible (1) or not (0)
        :param uid: Int, id of user who uploaded photo
        """
        test_photo = PhotoModel(photo_name, data, visible, uid)
        test_photo.save_to_db()
        saved_photo = test_photo.find_by_name(photo_name)

        assert saved_photo.photo_name == photo_name
        assert saved_photo.data == data
        assert saved_photo.visible == visible
        assert saved_photo.user_id == uid
        assert saved_photo.id is not 0
        assert saved_photo.id > 0