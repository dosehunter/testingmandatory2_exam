import pytest
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager  # pip install webdriver-manager
from selenium.webdriver.common.keys import Keys
import time
# https://www.lambdatest.com/blog/test-automation-using-pytest-and-selenium-webdriver/
# To run from root dir (testing-final-project):
# pytest code/test/interface/interface_testing.py -vvv

# Retrieve the driver and pass it to the class once
@pytest.fixture(scope="class", autouse=True)
def prepare_driver(request):
    """
    Setup selenium driver

    :param request: The method requesting the driver
    """
    print("Running setup\rRetrieving driver...")
    # Retrieve driver:
    driver = webdriver.Chrome(ChromeDriverManager().install())

    # Set the driver for the requesting class:
    request.cls.driver = driver

    # Test:
    yield

    # Close driver
    driver.close()


@pytest.mark.usefixtures("prepare_driver")
class TestApp:
    """
    Interface testing of the application

    """

    URL = "http://localhost"
    PORT = 5000
    PHOTO = "photo"

    # Go to home page before each test
    @pytest.fixture(autouse=True)
    def prep_page(self):
        """
        Setup for each test, go to home page
        """
        self.driver.get(self.page())

    def test_testpage(self):
        """
        Test our test page
        """
        self.driver.get(self.page("test_page"))
        assert self.driver.title == "TEST PAGE"
        assert self.driver.find_element_by_id("test_text").text == "THIS IS PURELY FOR TESTING ROUTES"

    @pytest.mark.parametrize("username, password, exp_title", [
        # Valid users:
        ("test_uname_1",  "test_pwd_1", "Home Page"),
        ("test_uname_2",  "test_pwd_2", "Home Page"),
        ("test_uname_3",  "test_pwd_3", "Home Page"),
        # Invalid users
        ("wrong_un_1",    "wrong_pw_1", "Testing"),
        ("wrong_un_2",    "wrong_pw_2", "Testing"),
    ])
    def test_login(self, username, password, exp_title):
        """
        Test login (NOTE! This is only visually inputting username an pw no functionality)

        :param username: String, username to test
        :param password: String, password corresponding to username
        """
        assert self.driver.title == "Testing"
        username_in = self.driver.find_element_by_id("in_username")
        password_in = self.driver.find_element_by_id("in_password")

        username_in.send_keys(username)
        #time.sleep(1)
        password_in.send_keys(password)

        password_in.send_keys(Keys.ENTER)
        assert self.driver.title == exp_title


    @pytest.mark.parametrize("photo_id", [
        (2),
        (12),
        (8),
        (13),
    ])
    def test_see_photo(self, photo_id):
        """
        Test to see if photo is loaded

        A page showing a photo will have a title with photo id
        :param photo_id: Int, the photo to view
        """
        a = self.driver.get(self.page(f"{self.PHOTO}/{photo_id}"))
        assert f"{photo_id}" in self.driver.title

    @pytest.mark.parametrize("photo_id, expected_title", [
        (133,   "TypeError"),
        (-1,    "404 Not Found"),
        (0,     "TypeError"),
    ])
    def test_see_photo_invalid(self, photo_id, expected_title):
        """
        Testing invalid photos

        :param photo_id: Int, id of non-existent photo
        :param expected_title: String, expected title
        """
        self.driver.get(self.page(f"{self.PHOTO}/{photo_id}"))
        assert expected_title in self.driver.title

    @pytest.mark.parametrize("username, password, exp_title, imgs, valid", [
        ("test_uname_1", "test_pwd_1", "Home Page", {"Photo 2":2, "Photo 13":13}, True),
        ("test_uname_3", "test_pwd_3", "Home Page", {"Photo 12":12, "Photo 2":2, "Photo 13":13}, True),
        ("test_uname_33", "test_pwd_33", "Testing", {}, False),
    ])
    def test_full_tour(self, username, password, exp_title, imgs, valid):
        """
        Test a user logging in, click some images and logout

        :param username: String, username of user
        :param password: String, password of user
        :param exp_title: String, exptected html title
        :param imgs: Dict, images and ids to be clicked
        :param valid: Boolean, whether user should be able to login or not
        """
        assert self.driver.title == "Testing"
        username_in = self.driver.find_element_by_id("in_username")
        password_in = self.driver.find_element_by_id("in_password")
        username_in.send_keys(username)
        password_in.send_keys(password)

        password_in.send_keys(Keys.ENTER)
        assert self.driver.title == exp_title

        if valid:
            # Click images
            for i in imgs:
                photo_link = self.driver.find_element_by_link_text(i)
                photo_link.click()
                assert f"{imgs[i]}" in self.driver.title
                self.driver.execute_script("history.back();")
            assert self.driver.title == exp_title
            logout_link = photo_link = self.driver.find_element_by_link_text("Logout")
            logout_link.click()
            assert self.driver.title == "Testing"

    def page(self, route=""):
        return f"{self.URL}:{self.PORT}/{route}"

    @pytest.mark.parametrize("route, expected", [
        ("",            f"{URL}:{PORT}/"),
        ("register",    f"{URL}:{PORT}/register"),
        ("auth",        f"{URL}:{PORT}/auth"),
        ("photo/2",     f"{URL}:{PORT}/photo/2"),
    ])
    def test_page(self, route, expected):
        """
        Testing whether we create correct routes

        :param route: String, route to test
        :param expected: String, expected route
        """
        assert self.page(route) == expected

    @staticmethod
    def log(msg):
        return f"[INTERFACE] {msg}"
