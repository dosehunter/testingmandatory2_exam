# pip install pytest-flask
# pip install pytest-flask-sqlalchemy
import pytest
from models.user import UserModel
from models.photo import PhotoModel
from app import create_app, db

# PHOTOS:
photo_1 = {'id':1, 'photo_name':'test_photo_1', 'data': open("code/test/Cat.jpg", 'rb'), 'visible':1, 'uid':1}
photo_2 = {'id':2, 'photo_name':'test_photo_2', 'data': open("code/test/abs_robot.jpg", 'rb'), 'visible':1, 'uid':2}
photo_3 = {'id':3, 'photo_name':'test_photo_3', 'data': open("code/test/Cat.jpg", 'rb'), 'visible':0, 'uid':3}
photo_4 = {'id':4, 'photo_name':'test_photo_4', 'data': open("code/test/abs_robot.jpg", 'rb'), 'visible':1, 'uid':3}

@pytest.fixture
def get_photos():
    return [photo_1, photo_2, photo_3, photo_4]

# USERS:
user_1 = {'id':1, 'username':'test_user_1', 'password':'test_password_1', 'email':'test_mail_1@gmail.com'}
user_2 = {'id':2, 'username':'TEST_USER_2', 'password':'TEST_PASSWORD_2', 'email':'TEST_MAIL_2@GMAIL.COM'}
user_3 = {'id':3, 'username':'Test_User_3', 'password':'Test_Password_3', 'email':'Test_Mail_3@Gmail.Com'}

@pytest.fixture
def get_users():
    return [user_1, user_2, user_3]

# Scope is module, so it will run once for all test
@pytest.fixture(scope='module')
def test_client():
    """
    Initializes a test flask app client
    """
    flask_app = create_app('flask_test.cfg')

    testing_client = flask_app.test_client()

    ctx = flask_app.app_context()
    ctx.push()

    yield testing_client

    ctx.pop()


@pytest.fixture(scope='module')
def init_database():
    """
    Initializes a test database and populates it with users and photos
    """
    db.create_all()

    ###########################################################################
       ### Populate database
    ###########################################################################
    ### Users:
    user1 = UserModel(user_1['username'], user_1['password'], user_1['email'])
    user2 = UserModel(user_2['username'], user_2['password'], user_2['email'])
    user3 = UserModel(user_3['username'], user_3['password'], user_3['email'])

    db.session.add(user1)
    db.session.add(user2)
    db.session.add(user3)

    ### Photos:
    photo1 = PhotoModel(photo_1['photo_name'], photo_1['data'].read(), photo_1['visible'], photo_1['uid'])
    db.session.add(photo1)

    photo2 = PhotoModel(photo_2['photo_name'], photo_2['data'].read(), photo_2['visible'], photo_2['uid'])
    db.session.add(photo2)

    photo3 = PhotoModel(photo_3['photo_name'], photo_3['data'].read(), photo_3['visible'], photo_3['uid'])
    db.session.add(photo3)

    photo4 = PhotoModel(photo_4['photo_name'], photo_4['data'].read(), photo_4['visible'], photo_4['uid'])
    db.session.add(photo4)

    db.session.commit()

    yield db

    # When test are over, drop all from our test_data.db
    db.drop_all()
